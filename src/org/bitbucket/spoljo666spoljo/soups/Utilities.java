package org.bitbucket.spoljo666spoljo.soups;

import java.io.File;

import org.bukkit.Bukkit;

public class Utilities {

	Main pl;

	public Utilities(Main pl) {
		this.pl = pl;
	}

	public void updateConfig() {
		File config = new File(pl.getDataFolder(), "config.yml");
		File old_config = new File(pl.getDataFolder(), "config_old.yml");
		config.renameTo(old_config);
		Bukkit.getLogger().info(String.format("[%s] Found an old config file, replacing it with a new one.", pl.getName()));
		pl.saveDefaultConfig();
	}
}