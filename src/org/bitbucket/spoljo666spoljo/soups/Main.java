package org.bitbucket.spoljo666spoljo.soups;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	boolean update = false;
	String name = "";
	long size = 0;
	double configVersion = 2.6;
	public String version;
	public Updater updateChecker;
	public Economy econ = null;
	public MessageUtils msgUtils;
	public Utilities utils;

	@Override
	public void onEnable() {
		saveDefaultConfig();

		version = getDescription().getVersion();
		msgUtils = new MessageUtils(this);
		utils = new Utilities(this);

		setupEconomy();
		checkConfigVersion();
		checkForUpdates();

		registerCommands();

		registerEvents(this, new HungerListener(this), new HealthListener(this));
	}

	public void checkForUpdates() {
		if (getConfig().getBoolean("update-checker")) {
			updateChecker = new Updater(this, 62927);
			registerEvents(this, new PlayerJoinUpdateListener(this));
		}
	}

	public void checkConfigVersion() {
		if (getConfig().getDouble("config-version", configVersion - 0.5) < configVersion) {
			utils.updateConfig();
		}
	}

	private boolean setupEconomy() {
		if (Bukkit.getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = Bukkit.getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	public void registerEvents(Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getPluginManager().registerEvents(listener, plugin);
		}
	}

	public void registerCommands() {
		getCommand("soups").setExecutor(new CommandsHandler(this));
		getCommand("refill").setExecutor(new CommandsHandler(this));
	}
}