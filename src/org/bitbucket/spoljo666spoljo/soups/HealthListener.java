package org.bitbucket.spoljo666spoljo.soups;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class HealthListener implements Listener {

	Main pl;

	public HealthListener(Main pl) {
		this.pl = pl;
	}

	@EventHandler
	public void onPlayerInteractHEALTH(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		if (pl.getConfig().getList("enabled-worlds").contains(p.getWorld().getName())) {
			if (p.getHealth() < p.getMaxHealth()) {
				if (pl.getConfig().getBoolean("health-regen-enabled")) {
					if (p.hasPermission("soups.fast-health") || !p.isOp()) {
						if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
							if (p.getItemInHand().getType() == Material.MUSHROOM_SOUP) {
								p.setHealth(p.getHealth() + pl.getConfig().getInt("health-regen") > p.getMaxHealth() ? p.getMaxHealth() : p.getHealth() + pl.getConfig().getInt("health-regen"));
								e.getItem().setType(Material.BOWL);
								try {
									p.getWorld().playSound(p.getLocation(), Sound.valueOf(pl.getConfig().getString("health-regen-sound").toUpperCase()), 1, 1);
								} catch (Exception e2) {
									Bukkit.getLogger().severe(String.format("[%s] Sound is missconfigured ! Check your config.yml !", pl.getName()));
								}
							}
						}
					}
				}
			}
		}
	}
}