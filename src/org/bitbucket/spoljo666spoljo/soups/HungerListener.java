package org.bitbucket.spoljo666spoljo.soups;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class HungerListener implements Listener {

	Main pl;

	public HungerListener(Main pl) {
		this.pl = pl;
	}

	@EventHandler
	public void onPlayerInteractHUNGER(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		Action a = e.getAction();
		if (pl.getConfig().getList("enabled-worlds").contains(p.getWorld().getName())) {
			if (p.getFoodLevel() < 20) {
				if (pl.getConfig().getBoolean("hunger-regen-enabled")) {
					if (p.hasPermission("soups.fast-hunger") || !p.isOp()) {
						if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
							if (p.getItemInHand().getType() == Material.MUSHROOM_SOUP) {
								p.setFoodLevel(p.getFoodLevel() + pl.getConfig().getInt("hunger-regen"));
								e.getItem().setType(Material.BOWL);
								try {
									p.getWorld().playSound(p.getLocation(), Sound.valueOf(pl.getConfig().getString("hunger-regen-sound").toUpperCase()), 1, 1);
								} catch (Exception e2) {
									Bukkit.getLogger().severe(String.format("[%s] Sound is missconfigured ! Check your config.yml !", pl.getName()));
								}
							}
						}
					}
				}
			}
		}
	}
}