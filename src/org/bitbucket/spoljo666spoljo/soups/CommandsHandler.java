package org.bitbucket.spoljo666spoljo.soups;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandsHandler implements CommandExecutor {

	Main pl;

	public CommandsHandler(Main pl) {
		this.pl = pl;
	}

	@Override
	public boolean onCommand(CommandSender s, Command cmd, String cl, String[] args) {
		if (cmd.getName().equalsIgnoreCase("soups")) {
			if (!s.hasPermission("soups.admin") || !s.isOp()) {
				pl.msgUtils.sendNoPermissionMessage(s);
				return true;
			}
			if (args.length == 0) {
				pl.msgUtils.sendHelpPage(s);
				return true;
			}
			if (args.length >= 1) {
				if (args[0].equalsIgnoreCase("help")) {
					pl.msgUtils.sendHelpPage(s);
					return true;
				}
				if (args[0].equalsIgnoreCase("reload")) {
					pl.reloadConfig();
					pl.msgUtils.sendReloadMessage(s);
					return true;
				}
				if (args[0].equalsIgnoreCase("sethunger")) {
					try {
						double amount = Integer.parseInt(args[1]);
						pl.getConfig().set("hunger-regen", amount);
						pl.saveConfig();
						pl.msgUtils.sendSetHungerRegenMessage(s);
					} catch (Exception e) {
						pl.msgUtils.translateColors(s, "&4Error: &cThe second argument must be a number (0 - 20)");
					}
					return true;
				}
				if (args[0].equalsIgnoreCase("sethealth")) {
					try {
						double amount = Double.parseDouble(args[1]);
						pl.getConfig().set("health-regen", amount);
						pl.saveConfig();
						pl.msgUtils.sendSetHealthRegenMessage(s);
					} catch (Exception e) {
						pl.msgUtils.translateColors(s, "&4Error: &cThe second argument must be a number (0.0 - 20.0)");
					}
					return true;
				}
				if (args[0].equalsIgnoreCase("fhunger")) {
					try {
						boolean value = Boolean.parseBoolean(args[1]);
						pl.getConfig().set("hunger-regen-enabled", value);
						pl.saveConfig();
						if (value == true) {
							pl.msgUtils.sendHungerRegenEnabledMessage(s);
							return true;
						}
						if (value == false) {
							pl.msgUtils.sendHungerRegenDisabledMessage(s);
							return true;
						}
					} catch (Exception e) {
						pl.msgUtils.translateColors(s, "&4Error: &cThe second argument must be a boolean (true / false)");
					}
					return true;
				}
				if (args[0].equalsIgnoreCase("fhealth")) {
					try {
						boolean value = Boolean.parseBoolean(args[1]);
						pl.getConfig().set("health-regen-enabled", value);
						pl.saveConfig();
						if (value == true) {
							pl.msgUtils.sendHealthRegenEnabledMessage(s);
							return true;
						}
						if (value == false) {
							pl.msgUtils.sendHealthRegenDisabledMessage(s);
							return true;
						}
					} catch (Exception e) {
						pl.msgUtils.translateColors(s, "&4Error: &cThe second argument must be a boolean (true / false)");
					}
					return true;
				}
				return true;
			}
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("refill")) {
			if (!(s instanceof Player)) {
				pl.msgUtils.sendOnlyPlayerMessage(s);
				return true;
			}
			Player p = (Player) s;
			if (!p.hasPermission("soups.refill") || !s.isOp()) {
				pl.msgUtils.sendNoPermissionMessage(p);
				return true;
			}
			if (args.length > 0) {
				pl.msgUtils.translateColors(p, "�4Correct usage: �c/refill");
			}
			int amount = pl.getConfig().getInt("refill.amount");
			double price = pl.getConfig().getDouble("refill.price-per-soup");
			boolean use_econ = pl.getConfig().getBoolean("refill.use-economy");
			String currency = pl.getConfig().getString("refill.currency");
			if (use_econ) {
				if (!pl.econ.has(p.getName(), amount * price)) {
					pl.msgUtils.sendNotEnoughMoneyMessage(p);
					return true;
				}
				if (p.getInventory().firstEmpty() == -1) {
					pl.msgUtils.sendFullyInvMessage(p);
					return true;
				}
				pl.econ.withdrawPlayer(p.getName(), amount * price);
				for (int i = 0; i < amount; i++) {
					p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP, 1));
				}
				String amountString = Integer.toString(amount);
				String priceString = Double.toString(price * amount);
				String message = ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("messages.successfully-refilled").replace("{price}", priceString + currency).replace("{amount}", amountString));
				p.sendMessage(message);
				return true;
			}
			if (p.getInventory().firstEmpty() == -1) {
				pl.msgUtils.sendFullyInvMessage(p);
				return true;
			}
			for (int i = 0; i < amount; i++) {
				p.getInventory().addItem(new ItemStack(Material.MUSHROOM_SOUP, 1));
			}
			String amountString = Integer.toString(amount);
			String message = ChatColor.translateAlternateColorCodes('&', pl.getConfig().getString("messages.successfully-refilled").replace("{price}", "'free'").replace("{amount}", amountString));
			p.sendMessage(message);
			return true;
		}
		return false;
	}
}