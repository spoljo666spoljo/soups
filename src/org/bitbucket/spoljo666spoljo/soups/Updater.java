package org.bitbucket.spoljo666spoljo.soups;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.bukkit.Bukkit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class Updater {

	Main pl;
	private final int projectID;
	private static final String API_NAME_VALUE = "name";
	private static final String API_LINK_VALUE = "downloadUrl";
	private static final String API_RELEASE_TYPE_VALUE = "releaseType";
	private static final String API_FILE_NAME_VALUE = "fileName";
	private static final String API_GAME_VERSION_VALUE = "gameVersion";

	private static final String API_QUERY = "/servermods/files?projectIds=";
	private static final String API_HOST = "https://api.curseforge.com";
	public boolean updateAvailable;

	public Updater(Main pl, int projectID) {
		this.pl = pl;
		this.projectID = projectID;
		updateAvailable = false;
		query();
	}

	public boolean isUpdateAvailable() {
		return updateAvailable;
	}

	public void query() {
		URL url = null;
		try {
			url = new URL(API_HOST + API_QUERY + projectID);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		}
		try {
			URLConnection conn = url.openConnection();
			conn.addRequestProperty("User-Agent", "ServerModsAPI-Example (by Gravity)");
			final BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String response = reader.readLine();
			JSONArray array = (JSONArray) JSONValue.parse(response);
			if (array.size() > 0) {
				JSONObject latest = (JSONObject) array.get(array.size() - 1);
				String versionName = (String) latest.get(API_NAME_VALUE);
				latest.get(API_LINK_VALUE);
				latest.get(API_RELEASE_TYPE_VALUE);
				latest.get(API_FILE_NAME_VALUE);
				latest.get(API_GAME_VERSION_VALUE);
				versionName = versionName.replaceAll("[a-zA-Z]", "");
				versionName = versionName.replaceAll(" ", "");
				if (!versionName.equals(pl.version)) {
					updateAvailable = true;
					Bukkit.getLogger().warning("==================================================");
					Bukkit.getLogger().info(String.format("[%s] There is an update available!", pl.getName()));
					Bukkit.getLogger().info(String.format("[%s] You can download it from: http://dev.bukkit.org/bukkit-plugins/soups/", pl.getName()));
					Bukkit.getLogger().warning("==================================================");
				} else {
					updateAvailable = false;
					Bukkit.getLogger().info(String.format("[%s] You are using the latest version! Thanks :)", pl.getName()));
				}
			} else {
				Bukkit.getLogger().info(String.format("[%s] No files found for this project.", pl.getName()));
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

}