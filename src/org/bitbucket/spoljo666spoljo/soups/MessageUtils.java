package org.bitbucket.spoljo666spoljo.soups;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MessageUtils {

	Main pl;

	public MessageUtils(Main pl) {
		this.pl = pl;
	}

	public void sendFullyInvMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.full-inventory"));
	}

	public void sendFullyInvMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.full-inventory"));
	}

	public void sendHealthRegenDisabledMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.health-regen-disabled"));
	}

	public void sendHealthRegenDisabledMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.health-regen-disabled"));
	}

	public void sendHealthRegenEnabledMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.health-regen-enabled"));
	}

	public void sendHealthRegenEnabledMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.health-regen-enabled"));
	}

	public void sendHelpPage(CommandSender s) {
		translateColors(s, String.format("&c=&6=&c=&6=&c=&6=&c=&6=&c=&6= &7%s v%s &c=&6=&c=&6=&c=&6=&c=&6=&c=&6=", pl.getDescription().getName(), pl.getDescription().getVersion()));
		translateColors(s, "&c/refill &e- &afills your inventory with soups &4<-NOT YET IMPLEMENTED");
		translateColors(s, "&c/soups help &e- &ashows this page");
		translateColors(s, "&c/soups reload &e- &areloads configuration file");
		translateColors(s, "&c/soups sethunger &e- &asets the new value for hunger regen");
		translateColors(s, "&c/soups sethealth &e- &asets the new value for health regen");
		translateColors(s, "&c/soups fhunger &e- &aenables / disables fast feeding");
		translateColors(s, "&c/soups fhealth &e- &aenables / disables fast healing");
	}

	public void sendHungerRegenDisabledMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.hunger-regen-disabled"));
	}

	public void sendHungerRegenDisabledMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.hunger-regen-disabled"));
	}

	public void sendHungerRegenEnabledMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.hunger-regen-enabled"));
	}

	public void sendHungerRegenEnabledMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.hunger-regen-enabled"));
	}

	public void sendNoPermissionMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.no-permission"));
	}

	public void sendNoPermissionMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.no-permission"));
	}

	public void sendNotEnoughMoneyMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.not-enough-money"));
	}

	public void sendNotEnoughMoneyMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.not-enough-money"));
	}

	public void sendOnlyPlayerMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.only-player-command"));
	}

	public void sendOnlyPlayerMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.only-player-command"));
	}

	public void sendReloadMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.reload"));
	}

	public void sendReloadMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.reload"));
	}

	public void sendSetHealthRegenMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.set-health-regen"));
	}

	public void sendSetHealthRegenMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.set-health-regen"));
	}

	public void sendSetHungerRegenMessage(CommandSender s) {
		translateColors(s, pl.getConfig().getString("messages.set-hunger-regen"));
	}

	public void sendSetHungerRegenMessage(Player p) {
		translateColors(p, pl.getConfig().getString("messages.set-hunger-regen"));
	}

	public void translateColors(CommandSender s, String message) {
		s.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}

	public void translateColors(Player p, String message) {
		p.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
	}
}