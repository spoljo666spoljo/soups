package org.bitbucket.spoljo666spoljo.soups;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinUpdateListener implements Listener {

	Main pl;

	public PlayerJoinUpdateListener(Main pl) {
		this.pl = pl;
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		if (p.hasPermission("soups.admin") || !p.isOp()) {
			if (pl.updateChecker.updateAvailable) {
				pl.msgUtils.translateColors(p, "&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=");
				pl.msgUtils.translateColors(p, "&6There is an update available for &7Soups");
				pl.msgUtils.translateColors(p, "&6You can download it from:");
				pl.msgUtils.translateColors(p, "&7http://dev.bukkit.org/bukkit-plugins/soups/");
				pl.msgUtils.translateColors(p, "&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=&c=&6=");
			}
		}
	}
}